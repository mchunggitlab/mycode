#!/usr/bin/python3
"""Alta3 Research | <your name here>
   Using an HTTP GET to determine when the ISS will pass over head"""

# python3 -m pip install requests
import requests

def main():
    """your code goes below here"""
    
    # stuck? you can always write comments
    # Try describe the steps you would take manually
    ## Trace the ISS - earth-orbital space station
    link  = 'http://api.open-notify.org/iss-pass.json?lat=47.6&lon=-122.3'
    url = 'http://api.open-notify.org/iss-now.json'
    ## Call the webserv
    #trackiss = urllib.request.urlopen(link)

    ## put into file object
    #ztrack = trackiss.read()

    ## JSON 2 Python data structure
    #result = json.loads(ztrack.decode('utf-8'))

    resp = requests.get(url)

    ## Decode the response
    data = resp.json()

    iss_latitude = float(data["iss_position"]["latitude"])
    iss_longitude = float(data["iss_position"]["longitude"])

    ## print the response
    print(link_res)

    while True:
        ulat = input('What latitude would you like to check?')
        ulon = input('What longitude would you like to check?')

        try:
            # -180 to 180 non inclusive
            # if float(ulat) > -180 and float(ulat) < 180
            if abs(float(ulat)) < 180 and abs(float(ulon)) < 180:
                r = resquests.get(f'http://api.open-notify.org/iss-pass.json?lat={ulat}&lon={ulon}')
                break
            else:
                print('Try those number again')

         except Exception as err:
             print("Check to make sure you provide valid input")
             print(err)
             continue
        
        print(r.status_code)
        data = r.json()
        next_overhead = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(data['response']['0']['risetime']))

        print(f"You will see ISS over your head at {next_overhead} and it will last for )
        
if __name__ == "__main__":
    main()

